
# Desafio Frontend React

**Tabela de Conteudos**

 - [Sobre o projeto](#sobre-o-projeto)
 - [Caracteristicas](#caracteristicas)
 - [Inicialização](#inicialização)
	- [Requisitos](#requisitos)
	- [Instalacao das dependencias](#instalacao-das-dependencias)
	- [Comandos de inicialização](#comandos-de-inicialização)
- [Estrutura das pastas](#estrutura-das-pastas)
- [Rotas](#rotas)

## Sobre o projeto

Este desafio trate-se de uma das etapas no processo seletivo para a [Mundiale](https://www.mundiale.com.br/). Esta aplicação tem como objetivo mostrar uma lista de funcionarios da Mundiale e quando selecionado mostrar seu cartao de visita. Feito usando React, este projeto traz consigo o uso de libs importantes para construção de uma aplicação segura.

## Caracteristicas
- [React](https://pt-br.reactjs.org/)
- SEO, SPA, PWA, Acessibilidade
- Testes End-to-end com [Cypress](http://cypress.io/)
- Testes unitario com [Jest](https://jestjs.io/) atraves da [Testing Library](https://testing-library.com/docs/react-testing-library/intro)
- Organização e padronização de códigos com [ESLint](https://github.com/eslint/eslint)
- Criação e utilização de variaveis de ambiente com o [dotEnv](https://github.com/motdotla/dotenv)
- Offline First



## Inicialização

### Requisitos
- [Node](https://nodejs.org/en/download/)
- [Yarn](https://yarnpkg.com/lang/en/docs/install) ou [NPM](https://www.npmjs.com/get-npm)

### Instalacao das dependencias

Para clonar este desafio em seu repositorio local, você ira precisar do Git, uma vez instalado corretamente você pode executar o comando:
```
$ git clone https://gitlab.com/iranjunior/mundiale.git
```

ou caso você tenha uma chave ssh configurada:
```
$ git clone git@gitlab.com:iranjunior/mundiale.git
```

Para instalar as dependencias deste projeto, com o terminal voce ira precisar entrar na pasta do projeto e executar o comando.
````
yarn
````

ou se preferir com o npm:

````
npm install
````


### Comandos de inicialização

Para iniciar a aplicação basta executar o comando: `yarn start` ou `npm run start`mas existe alguns comandos que podem lhe ajudar a realizar tarefas especificas como testes com a sua respectiva cobertura, cobertura dos testes e build da aplicação Uma lista completa sobre os comandos de inicialização pode ser vista abaixo:

Comandos  | Tarefa a ser realizada
------------- | -------------
`yarn start` | Inicializa o serviço em ambiente de desenvolvimento, com o modo live reload funcionando. O que facilita na atualização de componentes alterados em desenvolvimento
`yarn test` | Realiza todos os testes relacionados aos arquivos alterados
`yarn test:coverage`  | Realiza todos os testes e mostra os arquivos cobridos pelo teste
`yarn lint`  | Executa o ESLint nos arquivos da aplicação
`yarn build`  | Realiza o build da aplicação para execução em produção
`yarn storybook`  | Realiza a execucao do storybook
`yarn cypress`  | Realiza a execucao dos testes e2e com o cypress

## Estrutura das pastas
```
├─── .storybook/
   └───...
├─── cypress/
   └───...
├─── public/
   └───...
├─── src/
   ├─── assets/
     └───...
   ├─── components/
     └───...
   ├─── pages/
      └───...
   ├─── routes/
      └───...
   ├─── services/
       └───...
   ├─── stories/
       └───...
   ├─── styles/
       └───...
   ├─── utils/
       └───...
...
```


## Rotas

A aplicação tem apenas duas rotas disponíveis na aplicação. Uma hora de apresentação, onde é possível buscar o usuário do Github. E outra rota com informações do usuário buscado

### Tabela de rotas

As rotas da aplicação estão listadas na tabela abaixo:

Rota  |  Descrição
--------------------  | --------------
`GET /`  | Está é a rota raiz da aplicacao, esta rota resultara na pagina com a listagem dos funcionarios.
`GET /details/:id`  | Está é a rota que apresenta informações do funcionario buscado. Isso inclui sua foto e seu cartao.
