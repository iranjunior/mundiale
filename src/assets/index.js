import Person1 from './0.jpg';
import Person2 from './1.jpg';
import Person3 from './2.png';
import QRCode from './eduardo.png';
import Detail from './Group-174.svg';
import Logo from './Group-142.svg';
import Bar from './Group-130.svg';

export {
  Person1,
  Person2,
  Person3,
  QRCode,
  Detail,
  Logo,
  Bar,
};
