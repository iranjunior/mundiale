import React from 'react';
import { action } from '@storybook/addon-actions';
import Button from '../components/button';

export default {
  title: 'ButtonComponent',
  component: Button,
  decorators: [(storyFn) => <div style={{ width: '80px', height: '300px' }}>{storyFn()}</div>],
};

const props = {
  handleClick: action('clicked'),
  content: 'Click me',
  value: 'button',
};

export const Default = () => <Button {...props} />;
