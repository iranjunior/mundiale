import React from 'react';
import storyRouter from 'storybook-react-router';
import List from '../components/list';

export default {
  title: 'List',
  component: List,
  decorators: [storyRouter(), (storyFn) => <div style={{ backgroundColor: '#adf022', width: '100%' }}>{storyFn()}</div>],
  // decorators: [(storyFn) => <div style={{ width: '80px', height: '300px' }}>{storyFn()}</div>],
};

const content = [{
  id: 'azfulamsoqwtua',
  img: 'Person1',
  name: 'Eduardo Teste',
  description: 'UX design',
  email: 'eduardo.testes@mundiale.com',
},
{
  id: 'qiudbacxngqsdms',
  img: 'Person1',
  name: 'Evandro Julio',
  description: 'Front End',
  email: 'evandro.julio@mundiale.com',
}];

export const Default = () => <List content={content} />;
