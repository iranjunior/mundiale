import React from 'react';
import { action } from '@storybook/addon-actions';
import Slider from '../components/slider';

export default {
  title: 'Slider',
  component: Slider,
  decorators: [(storyFn) => (
    <div
      style={{
        width: '100%',
        height: '300px',
        display: 'flex',
        justifyContent: 'center',
        backgroundColor: '#11d7ff',
      }}
    >
      {storyFn()}

    </div>
  )],
};

const props = {
  letters: ['E', 'I', 'F'],
  handleClick: action('clicked'),
  offset: 1,
};

export const Default = () => <Slider {...props} />;
