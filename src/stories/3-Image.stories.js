import React from 'react';
import Image from '../components/image';

export default {
  title: 'Image',
  component: Image,
  decorators: [(storyFn) => <div style={{ width: '100px', height: '100px' }}>{storyFn()}</div>],
};

const props = {
  path: 'Person2',
  style: {
    borderRadius: '50%',
  },
};

// eslint-disable-next-line react/destructuring-assignment
export const Default = () => <Image path={props.path} />;
export const Round = () => <Image {...props} />;
