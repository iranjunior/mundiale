import React from 'react';
import Name from '../components/name';

export default {
  title: 'Name',
  component: Name,
};

const content = 'Iran Junior';
export const Default = () => <Name content={content} />;
