import React from 'react';
import Carte from '../components/carte';

export default {
  title: 'Carte',
  component: Carte,
};

const data = {
  email: 'eduardo.testes@mundiale.com',
};

export const Default = () => <Carte data={data} />;
