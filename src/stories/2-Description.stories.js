import React from 'react';
import Description from '../components/description';

export default {
  title: 'Description',
  component: Description,
};

const content = 'Frontend';
export const Default = () => <Description content={content} />;
