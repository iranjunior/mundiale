import React from 'react';
import { action } from '@storybook/addon-actions';
import ButtonIcon from '../components/buttonIcon';

export default {
  title: 'ButtonIcon',
  component: ButtonIcon,
  decorators: [(storyFn) => (
    <div style={{
      width: '40px',
      height: '40px',
      borderRadius: '50%',
      border: '1px solid grey',
      display: 'flex',
      justifyContent: 'center',
    }}
    >
      {storyFn()}
    </div>
  )],
};

const props = {
  handleClick: action('clicked'),
  icon: 'MdAdd',
  size: 20,
};

export const Default = () => <ButtonIcon {...props} />;
