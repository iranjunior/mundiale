import React from 'react';
import storyRouter from 'storybook-react-router';
import Card from '../components/cards';

export default {
  title: 'Card',
  component: Card,
  decorators: [storyRouter()],
  // decorators: [(storyFn) => <div style={{ width: '80px', height: '300px' }}>{storyFn()}</div>],
};

const data = {
  id: 'azfulamsoqwtua',
  img: 'Person1',
  name: 'Eduardo Teste',
  description: 'UX design',
  email: 'eduardo.testes@mundiale.com',
};

export const Default = () => <Card data={data} />;
