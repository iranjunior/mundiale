/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const Description = styled.p`
    font-size: ${(props) => props.theme.sizes.font.normal};
    color: ${(props) => props.color || props.theme.colors.font.normal};
    margin: 0;
`;
