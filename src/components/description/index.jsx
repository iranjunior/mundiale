import React from 'react';
import PropTypes from 'prop-types';

import { Description } from './styles';
import Boundary from '../erros';

const DescriptionComponent = ({ content, ...props }) => (
  <Boundary>
    <Description
      aria-label={content}
      {...props}
    >
      {content}
    </Description>
  </Boundary>
);
DescriptionComponent.propTypes = {
  content: PropTypes.string.isRequired,
};
export default DescriptionComponent;
