import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import Description from '../index';

import light from '../../../styles/themes/light';

describe('Test Descriptions', () => {
  it('Should render Description with success', () => {
    const content = 'Test';

    const wrapper = render(
      <ThemeProvider theme={light}>
        <Description content={content} />
      </ThemeProvider>,
    );

    expect(wrapper.queryByLabelText(content).firstChild.data).toBe(content);
  });
});
