/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import Carte from '../index';
import light from '../../../styles/themes/light';
import person from './payloads/person.json';

describe('Test buttons', () => {
  it('Should render Carte with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Carte
          data={person}
        />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.lastChild.firstChild.innerHTML).toContain(person.email);
  });
});
