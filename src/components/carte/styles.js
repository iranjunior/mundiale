import styled from 'styled-components';

export const Container = styled.div`
    height: 200px;
    margin-top: 30px;
    width: 80%;
    background-color: ${(props) => props.theme.colors.background.gradientSteps[1]};
    border-radius: 10px;
    display: flex;
    flex-direction: column;
`;
export const Header = styled.div`
    display: flex;
    height: 50%;
    padding: 10px;
}
`;
export const Images = styled.div`
    display: flex;
    height: 100%;
    width: 50%;
    overflow: hidden;
    justify-content: center;
    align-items: center;
    padding: 10px;
    padding-top: 0px;
`;
export const Numbers = styled.div`
    display: flex;
    width: 50%;
    flex-direction: column;
    flex-wrap: wrap;
    padding: 10px;
    padding-left: 20px;
    justify-content: center;
`;
export const Footer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 50%;
    flex-direction: column;

    & div {
        position: relative;
        bottom: -20px;
        height: 10px;
    }
`;
