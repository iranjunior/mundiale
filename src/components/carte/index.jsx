import React from 'react';
import PropTypes from 'prop-types';

import Description from '../description';
import Image from '../image';
import Wall from '../wall';
import {
  Container, Images, Header, Numbers, Footer,
} from './styles';
import Boundary from '../erros';


const Carte = ({ data }) => (
  <Boundary>
    <Container>
      <Header>
        <Images>
          <Image description="Logo" size="contain" path="Logo" />
        </Images>
        <Wall />
        <Numbers>
          <Description content="31 9.9790-7717" color="#fff" />
          <Description content="31 3456-0629" color="#fff" />
        </Numbers>
      </Header>
      <Footer>
        <Description content={data.email} color="#fff" />
        <Image path="Bar" description="Bar" />
      </Footer>
    </Container>
  </Boundary>
);

Carte.propTypes = {
  data: PropTypes.shape({
    email: PropTypes.string.isRequired,
  }).isRequired,
};

export default Carte;
