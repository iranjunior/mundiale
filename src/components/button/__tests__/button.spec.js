import React from 'react';
import { render } from '@testing-library/react';
import Button from '../index';

describe('Test buttons', () => {
  it('Should render button with success', () => {
    const content = 'Test';

    const wrapper = render(<Button value={content} content={content} />);

    expect(wrapper.queryByLabelText(content).firstChild.data).toBe(content);
  });

  it('Should faild render button', () => {
    const content = 'Test';

    const wrapper = render(<Button value={content} content={new Error('fail')} />);

    expect(wrapper.container.innerHTML).toContain('failed');
  });
});
