/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const Button = styled.button`
    font-size: 18px;
    font-weight: 500;
    color: #777;
`;
