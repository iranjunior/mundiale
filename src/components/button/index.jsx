import React from 'react';
import PropTypes from 'prop-types';
import { Button } from './styles';
import Boundary from '../erros';

const ButtonComponent = ({ value, content, handleClick }) => (
  <Boundary>
    <Button
      tabIndex={0}
      role="button"
      value={value}
      aria-label={content || value}
      onClick={handleClick}
    >
      {content}
    </Button>
  </Boundary>
);

ButtonComponent.defaultProps = {
  handleClick: () => {},
  value: 'button',
};

ButtonComponent.propTypes = {
  content: PropTypes.string.isRequired,
  value: PropTypes.string,
  handleClick: PropTypes.func,
};

export default ButtonComponent;
