import React, { memo } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import Name from '../name';
import Description from '../description';
import Image from '../image';
import Wall from '../wall';
import ButtonIcon from '../buttonIcon';

import Boundary from '../erros';

import {
  Container, Images, Content, Last,
} from './styles';


const Cards = ({ data }) => {
  const history = useHistory();

  const redirect = () => {
    history.push(`/details/${data.id}`);
  };

  return (
    <Boundary>
      <Container aria-label={data.name} role="button" onClick={() => redirect()}>
        <Images>
          <Image description={data.name} path={data.img} />
        </Images>
        <Content>
          <Name content={data.name} />
          <Description content={data.description} />
        </Content>
        <Wall />
        <Last>
          <ButtonIcon icon="MdAdd" size={20} />
        </Last>
      </Container>
    </Boundary>
  );
};

Cards.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
};

export default memo(Cards);
