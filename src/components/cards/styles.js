import styled from 'styled-components';

export const Container = styled.div`
    height: 80px;
    width: 100%;
    background-color: ${(props) => props.theme.colors.background.card};
    border-radius: 10px;
    display: flex;
`;
export const Images = styled.div`
    display: flex;
    height: 100%;
    width: 40%;
    border-radius: 10px 0px 0px 10px;
    overflow: hidden;
`;
export const Content = styled.div`
    display: flex;
    flex-direction: column;
    padding: 20px;
    justify-content: center;
    width: 70%;
`;
export const Last = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 20%;
`;
