/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render, fireEvent } from '@testing-library/react';
import Card from '../index';
import light from '../../../styles/themes/light';
import person from './payloads/person.json';

const mockPush = jest.fn();

jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: mockPush,
  }),
}));
describe('Test buttons', () => {
  it('Should render Card with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Card
          data={person}
        />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.firstChild.innerHTML).toContain(person.name);
  });

  it('Should render Card with success and redirect to details', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Card
          data={person}
        />
      </ThemeProvider>,
    );
    fireEvent.click(wrapper.getAllByRole('button')[0]);

    expect(mockPush).toHaveBeenCalled();
  });
});
