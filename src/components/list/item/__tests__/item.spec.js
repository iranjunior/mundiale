import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import Item from '../index';

import person from './payloads/person.json';
import light from '../../../../styles/themes/light';


jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('Test Items', () => {
  it('Should render Item with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Item data={person} />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.firstChild.firstChild.innerHTML)
      .toContain(person.name);
  });
});
