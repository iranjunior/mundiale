import React, {
  useCallback,
  memo,
} from 'react';
import PropTypes from 'prop-types';
import { Item } from './styles';
import Cards from '../../cards';
import Boundary from '../../erros';

const ItemComponent = ({ data }) => {
  const renderItems = useCallback(() => {
    if (data !== null) {
      return (
        <Cards data={data} />
      );
    }
    return null;
  }, [data]);
  return (
    <Boundary>
      <Item>
        {
        renderItems()
        }
      </Item>
    </Boundary>
  );
};

ItemComponent.propTypes = {
  data: PropTypes.shape({
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }),
};
ItemComponent.defaultProps = {
  data: null,
};
export default memo(ItemComponent);
