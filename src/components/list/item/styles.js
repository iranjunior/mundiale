/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const Item = styled.li`
width: 100%;
margin: 20px 0px;
height: 80px;
border-radius: 10px;
border: 2px solid #FFF;
box-shadow: ${(props) => (props.children === null ? 'none' : '5px 5px 5px 0px #777')};

&::after {
    content: '';
    display: block; 
    position: relative;
    bottom: ${(props) => (props.children === null ? '-50%' : '50%')};
    left: -20px;
    height: 10px;
    width: 10px;
    border-radius: 50%;
    background-color: #fff;
}
`;
