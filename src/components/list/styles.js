/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const List = styled.ul`
    height: 100%;
    width: 80%;
    padding: 0 20px;
    margin: 0;
    list-style: none;
    overflow-y: scroll;
`;
