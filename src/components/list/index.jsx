import React from 'react';
import PropTypes from 'prop-types';
import Item from './item';

import { List } from './styles';
import Boundary from '../erros';

const ListComponent = ({ content }) => {
  const minimal = 4;
  const renderItens = () => {
    if (content.length > minimal) {
      return content.map((data) => <Item key={data.name} data={data} />);
    }
    const elements = [];
    for (let counter = 0; counter <= minimal; counter += 1) {
      if (content[counter]) {
        elements.push(<Item key={counter} data={content[counter]} />);
      } else {
        elements.push(<Item key={counter} />);
      }
    }
    return elements;
  };
  return (
    <Boundary>
      <List>{renderItens()}</List>
    </Boundary>
  );
};
ListComponent.propTypes = {
  content: PropTypes.arrayOf(PropTypes.shape({
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  })).isRequired,
};
export default ListComponent;
