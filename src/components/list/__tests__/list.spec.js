import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import List from '../index';

import content from './payloads/content.json';
import multiple from './payloads/multiple.json';

import light from '../../../styles/themes/light';


jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('Test Lists', () => {
  it('Should render List with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <List content={content} />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.firstChild.firstChild.firstChild.innerHTML)
      .toContain(content[0].name);
  });
  it('Should render List with multiples datas with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <List content={multiple} />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.firstChild.firstChild.firstChild.innerHTML)
      .toContain(content[0].name);
  });
});
