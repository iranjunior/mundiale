import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import Image from '../index';

import person from './payloads/person.json';
import light from '../../../styles/themes/light';

describe('Test Images', () => {
  it('Should render Image with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Image path={person.img} description={person.name} />
      </ThemeProvider>,
    );

    expect(wrapper.container.innerHTML).toContain(person.name);
  });

  it('Should render Image from internet with success', () => {
    const url = 'https://via.placeholder.com/350x150';

    const wrapper = render(
      <ThemeProvider theme={light}>
        <Image path={url} description={person.name} />
      </ThemeProvider>,
    );

    expect(wrapper.container.innerHTML).toContain(person.name);
  });
});
