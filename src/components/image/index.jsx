import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import * as Images from '../../assets';

import { Image } from './styles';
import Boundary from '../erros';

const ImageComponent = ({ path, description, ...props }) => {
  const checkUrl = useCallback(() => {
    if (path.includes('http')) {
      return path;
    }
    return Images[path];
  }, [path]);

  return (
    <Boundary>
      <Image alt={description} {...props} image={checkUrl()} />
    </Boundary>
  );
};

ImageComponent.propTypes = {
  path: PropTypes.string.isRequired,
  description: PropTypes.string,
};
ImageComponent.defaultProps = {
  description: 'image',
};
export default ImageComponent;
