import styled from 'styled-components';

export const Container = styled.div``;
export const Image = styled.div`
    height: 100%;
    width: 100%;
    background-size: ${(props) => props.size || 'cover'};
    background-position: center;
    background-repeat: no-repeat;
    background-image: url(${(props) => props.image});
`;
