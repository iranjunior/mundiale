import React from 'react';

export default class Boundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  static getDerivedStateFromError() {
    return { error: true };
  }

  componentDidCatch() {

  }

  render() {
    if (this.state.error) {
      return <span>Ops, failed to load buttons, try reloading a page</span>;
    }
    return this.props.children;
  }
}
