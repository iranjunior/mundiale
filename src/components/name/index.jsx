import React from 'react';
import PropTypes from 'prop-types';
import { Name } from './styles';
import Boundary from '../erros';

const NameComponent = ({ content, ...props }) => (
  <Boundary>
    <Name
      aria-label={content}
      {...props}
    >
      {content}
    </Name>
  </Boundary>
);

NameComponent.propTypes = {
  content: PropTypes.string.isRequired,
};
export default NameComponent;
