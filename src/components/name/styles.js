/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const Name = styled.span`
    font-size: ${(props) => props.theme.sizes.font.large};
    font-weight: 500;
    color: ${(props) => props.color || props.theme.colors.font.normal};
    margin: 0;
`;
