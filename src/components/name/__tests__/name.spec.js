import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import Name from '../index';

import light from '../../../styles/themes/light';

describe('Test Names', () => {
  it('Should render Name with success', () => {
    const content = 'Test';

    const wrapper = render(
      <ThemeProvider theme={light}>
        <Name content={content} />
      </ThemeProvider>,
    );

    expect(wrapper.queryByLabelText(content).firstChild.data).toBe(content);
  });
});
