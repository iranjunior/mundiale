import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import Wall from '../index';

import light from '../../../styles/themes/light';

describe('Test Walls', () => {
  it('Should render Wall with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Wall />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.localName).toBe('hr');
  });
});
