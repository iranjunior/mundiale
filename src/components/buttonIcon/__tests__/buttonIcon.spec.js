/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import ButtonIcon from '../index';
import light from '../../../styles/themes/light';

describe('Test buttons', () => {
  it('Should render buttonIcon with success', () => {
    const icon = 'MdAdd';

    const wrapper = render(
      <ThemeProvider theme={light}>
        <ButtonIcon
          icon={icon}
        />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.lastChild.localName).toBe('svg');
  });
});
