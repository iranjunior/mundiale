/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const Button = styled.button`
    color: ${(props) => props.theme.colors.font.normal};
    background-color: transparent;
    border: none;
}
`;
