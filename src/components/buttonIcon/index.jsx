import React from 'react';
import PropTypes from 'prop-types';
import { getIcons } from '../../utils';
import { Button } from './styles';

import Boundary from '../erros';

const ButtonIconComponent = ({
  icon, content, handleClick, ...props
}) => (
  <Boundary>
    <Button onClick={handleClick} value={icon.replace(/Md/i, '')}>
      {getIcons(icon, props)}
      {content}
    </Button>
  </Boundary>
);

ButtonIconComponent.defaultProps = {
  handleClick: () => {},
  content: null,
};

ButtonIconComponent.propTypes = {
  icon: PropTypes.string.isRequired,
  content: PropTypes.string,
  handleClick: PropTypes.func,
};
export default ButtonIconComponent;
