/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const Slider = styled.ul`
    height: 100%;
    width: 40%;
    display: flex;
    list-style: none;
    padding: 0;
    margin: 0;
}

`;

export const Element = styled.li`
    height: 100%;
    width: ${(props) => (props.destak ? '80%' : '15%')};
    ${(props) => props.first && 'transform: translateX(12%);'}
    ${(props) => props.last && 'transform: translateX(-6%);'}
    display: flex;
    justify-content: center;
    align-items: center;
    font-weight: 200;
    padding: 10px 0px;
    font-size: ${(props) => props.theme.sizes.font.extraLarge};
    color: ${(props) => props.theme.colors.font.destak};
    transition: all 100ms ease; 
`;
