import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render, fireEvent } from '@testing-library/react';
import Slider from '../index';

import content from './payloads/content.json';
import light from '../../../styles/themes/light';

describe('Test Sliders', () => {
  it('Should render Slider with success', () => {
    const handleClick = jest.fn(() => {});
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Slider handleClick={handleClick} letters={content} />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.firstChild.innerHTML).toBe(content[0]);
  });

  it('Should render Slider and element click with success', () => {
    const handleClick = jest.fn(() => {});
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Slider handleClick={handleClick} letters={content} />
      </ThemeProvider>,
    );

    fireEvent.click(wrapper.getAllByRole('button')[0]);

    expect(handleClick).toHaveBeenCalled();
  });
});
