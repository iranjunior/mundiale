import React from 'react';
import PropTypes from 'prop-types';
import { Slider, Element } from './styles';
import Boundary from '../erros';

const SliderComponent = ({ handleClick, offset, letters }) => {
  const destak = offset;

  const renderOptions = () => letters
    .slice(destak === 0 ? destak : destak - 1, destak === 0 ? destak + 2 : destak + 2)
    .map((letter, index) => (
      <Boundary
        key={letter}
      >
        <Element
          destak={destak === 0 ? index === 0 : index === 1}
          first={destak === 0}
          last={destak === letters.length - 1}
          onClick={() => handleClick(index)}
          key={letter}
          role="button"
        >
          {letter}
        </Element>
      </Boundary>
    ));
  return (
    <Boundary>
      <Slider>
        {renderOptions()}
      </Slider>
    </Boundary>
  );
};

SliderComponent.propTypes = {
  offset: PropTypes.number,
  letters: PropTypes.arrayOf(PropTypes.string),
  handleClick: PropTypes.func.isRequired,
};

SliderComponent.defaultProps = {
  offset: 0,
  letters: [],
};

export default SliderComponent;
