import React from 'react';
import { ThemeProvider } from 'styled-components';
import { light } from './styles/themes';
import Routes from './routes';
import GlobalStyles from './styles/globalStyles';


const App = () => (
  <ThemeProvider theme={light}>
    <GlobalStyles />
    <Routes />
  </ThemeProvider>
);

export default App;
