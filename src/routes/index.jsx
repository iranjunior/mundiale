
import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import history from './history';
import Home from '../pages/home';
import Details from '../pages/details';

const Routes = () => (
  <BrowserRouter history={history}>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/details/:id" exact component={Details} />
    </Switch>
  </BrowserRouter>

);

export default Routes;
