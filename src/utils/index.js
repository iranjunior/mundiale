/* eslint-disable import/prefer-default-export */
import React from 'react';
import { useLocation } from 'react-router-dom';
import * as Icons from 'react-icons/md';

export const getIcons = (icon, props) => {
  if (icon.trim()) {
    return React.createElement(Icons[icon], { ...props });
  }
  return null;
};
export const useQuery = () => new URLSearchParams(useLocation().search);

export const calcuteDestak = (letters, setDestak) => (index) => {
  setDestak((prev) => {
    if (prev < letters.length - 1 && index > 0) {
      return prev + 1;
    }
    if (prev > 0 && index === 0) {
      return prev - 1;
    }
    return prev;
  });
};
