export default {
  name: 'dark',
  base: 'dark',

  colors: {
    background: {
      initial: '#18C2E1',
      final: '#50BF87',
    },
    font: {
      destak: '#fff',
      normal: '#A8ABA8',
    },
  },
  sizes: {
    font: {
      large: '18px',
      normal: '16px',
    },
  },
};
