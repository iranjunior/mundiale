import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import Home from '../index';

import data from './payloads/data.json';

import { calcuteDestak, useQuery } from '../../../utils';

import light from '../../../styles/themes/light';

jest.mock('../../../utils');
jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('Test Home Page', () => {
  it('Should render Home with success', () => {
    const mockCalcute = calcuteDestak.mockImplementation(() => () => data);
    const mockUseQuery = useQuery.mockImplementation(() => ({ get: () => undefined }));

    const wrapper = render(
      <ThemeProvider theme={light}>
        <Home />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.firstChild.firstChild.firstChild.innerHTML).toBe('E');
    expect(mockCalcute.mock.calls[1][0][0]).toBe('E');
    expect(mockUseQuery).toHaveBeenCalled();
  });

  it('Should render Home and params offset with success', () => {
    const mockCalcute = calcuteDestak.mockImplementation(() => () => data);
    const mockUseQuery = useQuery.mockImplementation(() => ({ get: () => 'I' }));

    const wrapper = render(
      <ThemeProvider theme={light}>
        <Home />
      </ThemeProvider>,
    );

    expect(wrapper.container.firstChild.firstChild.firstChild.firstChild.innerHTML).toBe('E');
    expect(mockCalcute.mock.calls[1][0][0]).toBe('E');
    expect(mockUseQuery).toHaveBeenCalled();
  });
});
