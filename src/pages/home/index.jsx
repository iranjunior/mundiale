import React, { useEffect, useState, useMemo } from 'react';

import Slider from '../../components/slider';
import List from '../../components/list';

import { useQuery, calcuteDestak } from '../../utils';
import { Container, Header, Content } from './styles';

import data from '../../data.json';
import Boundary from '../../components/erros';

const Home = () => {
  const query = useQuery();
  const [value, setValue] = useState({});
  const [destak, setDestak] = useState(0);

  const groupsData = useMemo(() => data.reduce((a, b) => ({
    ...a,
    [b.name.charAt(0).toLocaleUpperCase()]: [...(a[b.name.charAt(0).toLocaleUpperCase()] || []),
      b],
  }), {}), []);

  useEffect(() => {
    setValue(groupsData);
    if (query.get('offset')) { setDestak(Object.keys(groupsData).indexOf(query.get('offset'))); }
  }, []);

  return (
    <Boundary>
      <Container>
        <Header>
          <Slider
            handleClick={calcuteDestak(Object.keys(value), setDestak)}
            offset={destak}
            letters={Object.keys(value)}
          />
        </Header>
        <Content>
          <List content={value[Object.keys(value)[destak]] || []} />
        </Content>
      </Container>
    </Boundary>
  );
};

export default Home;
