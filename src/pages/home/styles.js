import styled, { keyframes } from 'styled-components';
import { Detail } from '../../assets';

const show = keyframes`
    0% {
        transform: translateX(100%);
    }
    100% {
        transform: translateX(0%);
    }
`;


export const Container = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    justify-content: center;
    background: linear-gradient(
        20deg,
        ${(props) => props.theme.colors.background.initial},
        ${(props) => props.theme.colors.background.final}
    )  
`;
export const Content = styled.div`
    display: flex;
    height: 80%;
    width: 100%;
    position: absolute;
    bottom: 0;
    justify-content: center;
    animation: ${show} 1s;
    animation-iteration-count: 1;
`;
export const Header = styled.div`
    display: flex;
    height: 20%;
    width: 100%;
    justify-content: center;
    background-image: url(${Detail});
    background-size: cover;
    background-repeat: no-repeat;
    background-position-y: bottom;

`;
