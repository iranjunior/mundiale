import styled from 'styled-components';


export const Container = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    justify-content: center;
    background: linear-gradient(
        180deg,
        ${(props) => props.theme.colors.background.initial},
        ${(props) => props.theme.colors.background.final}
    )  
`;
export const Content = styled.div`
    display: flex;
    height: 80%;
    width: 100%;
    position: absolute;
    bottom: 0;
    justify-content: flex-start;
    flex-direction: column;
    align-items: center;

    & span {
        margin-top: 30px;
    }
`;
export const Header = styled.div`
    display: flex;
    height: 15%;
    width: 100%;
    justify-content: flex-start;
    align-items: flex-start;
    background-color: ${(props) => props.theme.colors.background.gradientSteps[0]};

    & button {
        margin-top: 20px;
    }
`;
export const ImageSpacing = styled.div`
    position: absolute;
    top: 7%;
    left: calc(50% - 50px);
    height: 100px;
    width: 100px;
    border-radius: 50%;

    & div {
    height: 100%;
    width: 100%;
    border-radius: 50%;
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    box-shadow: 0px 0px 5px 0px #777;
    }
`;
export const Footer = styled.div`
    height: 20%;
    width: 100%;
    position: absolute;
    bottom: 0;
    background-color: ${(props) => props.theme.colors.background.gradientSteps[1]};
`;

export const QRCodeSpacing = styled.div`
    height: 150px;
    width: 150px;
    padding: 20px;
    background-color: #fff;
    position: relative;
    bottom: 60%;
    left: calc(50% - 90px);
    border-radius: 10px;
`;
