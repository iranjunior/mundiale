import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';

import data from '../../data.json';

import Name from '../../components/name';
import Description from '../../components/description';
import Image from '../../components/image';
import ButtonIcon from '../../components/buttonIcon';
import Carte from '../../components/carte';

import {
  Container, Header, Content, ImageSpacing, Footer, QRCodeSpacing,
} from './styles';
import Boundary from '../../components/erros';

const Home = () => {
  const [pearson, setPearson] = useState({});
  const history = useHistory();
  const { id } = useParams();

  useEffect(() => {
    setPearson(data.filter((info) => info.id === id)[0]);
  }, [id]);

  const goBack = () => {
    history.push(`/?offset=${pearson.name.charAt(0).toLocaleUpperCase()}`);
  };
  if (Object.keys(pearson).length === 0) {
    return null;
  }
  return (
    <Boundary>
      <Container>
        <Header>
          <ButtonIcon icon="MdChevronLeft" handleClick={goBack} size={22} color="#fff" />
          <ImageSpacing>
            <Image description={pearson.name} path={pearson.img || ''} />
          </ImageSpacing>
        </Header>
        <Content>
          <Name
            content={pearson.name || ''}
            color="#fff"
          />
          <Description
            content={pearson.description || ''}
            color="#fff"
          />
          <Carte data={pearson} />
        </Content>
        <Footer>
          <QRCodeSpacing>
            <Image path="QRCode" description="QRCode" />
          </QRCodeSpacing>
        </Footer>
      </Container>
    </Boundary>
  );
};

export default Home;
