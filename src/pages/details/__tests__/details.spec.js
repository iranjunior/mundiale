import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render, fireEvent } from '@testing-library/react';
import Details from '../index';

import light from '../../../styles/themes/light';

const mockPush = jest.fn();

jest.mock('../../../utils');
jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: mockPush,
  }),
  useParams: () => ({ id: 'azfulamsoqwtua' }),
}));

describe('Test Details Page', () => {
  it('Should render Details with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Details />
      </ThemeProvider>,
    );
    fireEvent.click(wrapper.getByRole('button'));
    expect(wrapper.container.firstChild.firstChild.lastChild.innerHTML).toContain('Eduardo Teste');
  });
  it('Should render Details and click in back with success', () => {
    const wrapper = render(
      <ThemeProvider theme={light}>
        <Details />
      </ThemeProvider>,
    );
    fireEvent.click(wrapper.getByRole('button'));
    expect(mockPush).toHaveBeenCalled();
  });
});
