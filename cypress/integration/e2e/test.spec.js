
describe('Access Site', () => {
  context('Test initial', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    });
    
    it('Should load page', () => {
      cy.get('[role="button"]').first().should(($button) => {
        expect($button).to.contain("E");
      })
      cy.get('[role="button"]').last().should(($card) => {
        expect($card).to.contain("Evandro");
      })
    });
    it('Should change page', () => {
      cy.get('[role="button"]').first().next().click();
      cy.get('[role="button"]').last().should(($card) => {
        expect($card).to.contain("Iran");
      })
    })
  });
});
describe('Access Site and click in details', () => {

  context('Details', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
      cy.get('[role="button"]').first().next().click();
      cy.get('[role="button"]').last().click();
      })
      it('Should show details', () => {
        cy.get('[aria-label="Iran Junior"]').should(($image) => {
          expect($image).to.contain("Iran Junior")
        })
      });
      it('Should back to list', () => {
        cy.get('[value="ChevronLeft"]').first().click();
        cy.get('[role="button"]').first().next().should(($button) => {
          expect($button).to.contain("I");
        })
      });
    });
});
